﻿using System;

public class Student
   {
        public string Name;

        public int Age;

        public Group group;

        public Faculty faculty;


        public Student(string name, int age, Group group, Faculty faculty)
        {
            this.Age = age;
            this.Name = name;
            this.group = group;
            this.faculty = faculty;
        }
    
}
