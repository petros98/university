﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University
{
    class Group : IDisplay
    {
        public string number { get; set; }
        //public Faculty faculty { get; set; }
        public Faculty faculty;
        public List<Student> students = new List<Student>();

        public Group(string number, Faculty faculty)
        {
            this.number = number;
            this.faculty = faculty;
        }
        public void AddStudent(string name, int age)
        {
            students.Add(new Student(name, age, this, this.faculty));
        }
        public void IDisplay()
        {
            //foreach (Student s in students)
            //{
            //    Console.WriteLine(s.Name + "  " + s.Age + "  " + s.group);
            //}
        }
    }
}
