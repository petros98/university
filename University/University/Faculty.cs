﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University
{
    class Faculty : IDisplay
    {
        public string name;
        public List<Group> groups= new List<Group>();
        public Faculty(string name)
        {
            this.name = name;
        }
        public void AddGroup(Group group) {
           // Group newGroup = new Group(group_name);
            groups.Add(group);
            //return newGroup;
        }
        public void IDisplay()
        {
            foreach (Group g in groups)
            {
                Console.WriteLine(g.number);
            };
        }
    }
}
