﻿using System;
using System.Collections.Generic;

namespace University
{
    class Program
    {
        static void Main(string[] args)
        {
            University university=new University("NPUA");
            Faculty ikm = university.addFaculty("ikm");
            Group first = new Group("first", ikm);
            ikm.AddGroup(first);
            first.AddStudent("student1", 21);
            first.AddStudent("Vaspur", 55);

            Console.WriteLine(university.name);
            foreach (var fac in university.faculties)
            {
                Console.WriteLine(fac.name);
                foreach (var group in fac.groups)
                {
                    Console.WriteLine(group.number);
                    foreach (var student in group.students)
                    {
                        Console.WriteLine(student.Name);
                        Console.WriteLine(student.Age);
                    }
                }
            }
        }
    }
    interface IDisplay
    {
        public void IDisplay();
    }
    class University
    {
        
        public string name;
        public List<Faculty> faculties = new List<Faculty>();
        public University(string name)
        {
            this.name = name;

        }

        public Faculty addFaculty(string fac_name) {
            Faculty newFac = new Faculty(fac_name);
            faculties.Add(newFac);
            return newFac;
        }
        //public List<Student> students;
    }    
        
        
}

            //for (int i = 0; i < 10; i++)
            //{
            //    university.faculties.Add(new Faculty("ff" + i));
            //    Console.WriteLine(university.faculties[i].name);
            //    for (int j = 0; j < 10; j++)
            //    {
            //        university.faculties[i].groups.Add(new Group("gg" + j));
            //        Console.WriteLine(university.faculties[i].groups[j].number);
            //        for (int k = 0; k < 10; k++)
            //        {
            //            university.faculties[i].groups[j].students.Add(new Student("usanox" + k*j*i, k));
            //            Console.WriteLine(university.faculties[i].groups[j].students[k].Name);
            //        }
            //    }